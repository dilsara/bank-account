import { Injectable } from "@angular/core";
import { API } from '../constant/api';

/**
 * @author Nuwan Dilsara
 * ICT Assistant - Department of Pension
 * 2020.05.15
 */

@Injectable()
export class BaseService{
    public api: API;
}
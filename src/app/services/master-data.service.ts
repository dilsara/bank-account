import { Injectable } from '@angular/core';
import { API } from '../constant/api';

@Injectable({
  providedIn: 'root'
})
export class MasterDataService {

  constructor(
    private _api: API
  ) { }
}

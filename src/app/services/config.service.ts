import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

/**
 * @author Nuwan Dilsara
 * ICT Assistant - Department of Pension
 * 2020.05.15
 */

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  // active this token for developing
  token: string = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxMzQ3MCIsImlhdCI6MTU4OTUzMzQ3Niwic3ViIjoiYXV0aF90b2tlbiIsImlzcyI6InBlbnNpb25kcHQiLCJiIjoiUEJ1c2VyIiwiYSI6IkJBTktfQUNDT1VOVF9DSEFOR0VSIiwiZCI6NzEzNSwiZXhwIjoxNTg5NTYyMjc2fQ.KuGUeshnoGAp5kdoxlND10LIAfcyT-CyUEpNQKRt1wo"

  constructor(
    private _http: HttpClient
  ) { }

  /**
   * get without token
   * @param url 
   */
  get(url: string){
    return this._http.get(url);
  }

  getWithToken(url: string){
    const httpOption = {
      headers : new HttpHeaders({
        // 'token' : localStorage.getItem("token")
        'token' : this.token
      })
    }
  }

}

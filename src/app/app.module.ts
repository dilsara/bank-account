import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AlertModule } from './_alert';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { MainComponent } from './component/main/main.component';
import { HeaderComponent } from './component/templates/header/header.component';
import { FooterComponent } from './component/templates/footer/footer.component';
import { NavigationComponent } from './component/navigation/navigation.component';
import { SearchPensionerComponent } from './component/search-pensioner/search-pensioner.component';
import { LocationNavigationComponent } from './component/dashboard/location-navigation/location-navigation.component';
import { PensionerDetailsComponent } from './component/pensioner-details/pensioner-details.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NewAccountDetailsComponent } from './component/new-account-details/new-account-details.component';
import { ListAccountComponent } from './component/list-account/list-account.component';
import { EditAccountDetailsComponent } from './component/edit-account-details/edit-account-details.component';
import { ViewAccountDetailsComponent } from './component/view-account-details/view-account-details.component';
import { AccountDetailsReportComponent } from './component/reports/account-details-report/account-details-report.component';
import { SourceDocumentComponent } from './component/reports/source-document/source-document.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    MainComponent,
    HeaderComponent,
    FooterComponent,
    NavigationComponent,
    SearchPensionerComponent,
    LocationNavigationComponent,
    PensionerDetailsComponent,
    NewAccountDetailsComponent,
    ListAccountComponent,
    EditAccountDetailsComponent,
    ViewAccountDetailsComponent,
    AccountDetailsReportComponent,
    SourceDocumentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AlertModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
      preventDuplicates: true
    }) // ToastrModule added
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

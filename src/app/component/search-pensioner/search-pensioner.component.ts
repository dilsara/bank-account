import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-pensioner',
  templateUrl: './search-pensioner.component.html',
  styleUrls: ['./search-pensioner.component.css']
})
export class SearchPensionerComponent implements OnInit {

  constructor(
    private _route : Router
  ) { }

  ngOnInit(): void {

  }

  
  searchPensioner(){
      this._route.navigateByUrl('/pensioner-details');
  }

}

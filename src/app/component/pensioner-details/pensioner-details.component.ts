import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../_alert';
import { $ } from 'protractor';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pensioner-details',
  templateUrl: './pensioner-details.component.html',
  styleUrls: ['./pensioner-details.component.css']
})
export class PensionerDetailsComponent implements OnInit {

  isLoadingPage: boolean;
  addedAccount: boolean;
  isSaved: boolean;

  options = {
    autoClose: false,
    keepAfterRouteChange: false
  };

  constructor(
    protected alertService: AlertService,
    private _route: Router
  ) { }

  ngOnInit(): void {
    this.isLoadingPage = true;
    this.addedAccount = false;
    this.isSaved = false;
    // load services
    this.isLoadingPage = false;
  }

  addNewAccount() {
    this.isLoadingPage = true;
    // content
    this.addedAccount = true;
    this.isLoadingPage = false;
  }

  addNewAccDetails() {
    this._route.navigateByUrl('/new-account-details');
    // this.alertService.success('Successfully Changed Account!!', 'options');
    // this.options.autoClose = true;
    // alert('Successfully Saved!');
    // this.isSaved = true;
  }

}

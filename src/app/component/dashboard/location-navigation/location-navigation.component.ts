import { Component, OnInit, Input } from '@angular/core';

/**
 * @author Nuwan Dilsara
 * ICT Assistant - Department of Pension
 * 2020.05.13
 */

@Component({
  selector: 'app-location-navigation',
  templateUrl: './location-navigation.component.html',
  styleUrls: ['./location-navigation.component.css']
})
export class LocationNavigationComponent implements OnInit {

  @Input() routerUrl;
  @Input() title;
  @Input() status;
  @Input() count;
  @Input() tile;
  @Input() tilecolor;
  @Input() id;
  @Input() description;

  @Input() color;
  


  constructor() { }

  ngOnInit(): void {
  }

}

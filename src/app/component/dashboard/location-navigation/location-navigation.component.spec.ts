import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationNavigationComponent } from './location-navigation.component';

describe('LocationNavigationComponent', () => {
  let component: LocationNavigationComponent;
  let fixture: ComponentFixture<LocationNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

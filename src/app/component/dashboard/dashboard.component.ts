import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  dashboard_config: any;

  dashboardTiles = [
      // {
      // "accessID": "db-1", "name": "Pending Accounts", "pathTo": "ndfcfgfdgk", "icon": "sdkjsksddssd", "colorCode": "#0072C6"
      // }
  
      // ,
      // {
      //   "accessID": "db-2", "name": "Approved Accounts", "pathTo": "ndfcfgfdgk", "icon": "sdkjsksddssd", "colorCode": "#0072C6"
      // }
  
      // ,
      // {
      //   "accessID": "db-3", "name": "Rejected Accounts", "pathTo": "/department-vew/list/pending", "icon": "sdkjsksddssd", "colorCode": "#0072C6"
      // }
  ];


  constructor() { 
  }

  ngOnInit(): void {

    // this should be goes to login
    this.dashboard_config = [
      {
        "pathTo": "/list-account",
        "name": "Pending",
        "reason":"pending",
        "tileColor" : "bg-info",
        "id": "tiletickets",
        "description": "Pending bank account for approve",
        "count": "10"
      },
      {
        "pathTo": "/list-account",
        "name": "Approved",
        "reason":"approved",
        "tileColor" : "bg-success",
        "id": "tilerevenues",
        "description": "Approved bank account details",
        "count": "20"
      },
      {
        "pathTo": "/list-account",
        "name": "Rejected",
        "reason":"rejected",
        "tileColor" : "bg-danger",
        "id": "tileorders",
        "description": "Rejected bank account details",
        "count": "30"
      }
    ]

    this.dashboardTiles = this.dashboard_config;
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BankModel } from 'src/app/models/bank.model';
import { BranchModel } from 'src/app/models/branch.model';
import { ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-new-account-details',
  templateUrl: './new-account-details.component.html',
  styleUrls: ['./new-account-details.component.css']
})
export class NewAccountDetailsComponent implements OnInit {

  banks: BankModel[] = [
    { id: 0, bankNo: 256,  bankName: 'BOC' },
    { id: 1, bankNo: 100,  bankName: 'PB' },
    { id: 2, bankNo: 967,  bankName: 'NSB' }
  ];

  
  branches: BranchModel[] = [
    { id: 1, name: 'Matugama' },
    { id: 2, name: 'Kalutara' },
    { id: 3, name: 'Panadura' }
  ];

  constructor(
    private _route: Router,
    private _toastr: ToastrService
  ) { }

  ngOnInit(): void {}

  saveAccDetails() {
    // save service
    this._toastr.success('Successfully changed Account Details.', 'Success!');
    this._route.navigateByUrl('/dashboard');
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountDetailsReportComponent } from './account-details-report.component';

describe('AccountDetailsReportComponent', () => {
  let component: AccountDetailsReportComponent;
  let fixture: ComponentFixture<AccountDetailsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountDetailsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountDetailsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

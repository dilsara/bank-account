import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PensionerModel } from 'src/app/models/pensioner.model';
import { BankModel } from 'src/app/models/bank.model';

@Component({
  selector: 'app-account-details-report',
  templateUrl: './account-details-report.component.html',
  styleUrls: ['./account-details-report.component.css']
})
export class AccountDetailsReportComponent implements OnInit {

  isLoadingPage: boolean;

  
  banks: BankModel[] = [
    { id: 0, bankNo: 256,  bankName: 'BOC' },
    { id: 1, bankNo: 100,  bankName: 'PB' },
    { id: 2, bankNo: 967,  bankName: 'NSB' }
  ];

  pensioners: PensionerModel[] = [
    {
      pid: 1234,
      name: "Karannagoda Mudalige Saman Jayathilaka",
      nic: "882892857V",
      penno: "1524254",
      bank: { id: 1, name: "Bank of Ceylon" },
      branch: { id: 254, name: "Matugama" },
      accountNo: "2445455"
    },
    {
      pid: 1234,
      name: "Induruwe Udumullage Nuwan Dilsara",
      nic: "198828902808",
      penno: "155214",
      bank: { id: 1, name: "People's Bank" },
      branch: { id: 254, name: "Kalutara" },
      accountNo: "2445455"
    },
    {
      pid: 1234,
      name: "Jothipathirathnage Iresha Udeyanganie Jothipathirathne",
      nic: "197528902524",
      penno: "15246546",
      bank: { id: 1, name: "National Savings Bank" },
      branch: { id: 254, name: "Panadura" },
      accountNo: "2445455"
    }
  ];

  status: [
    { id: 1, option: 'Pending' },
    { id: 2, option: 'Approved' },
    { id: 3, option: 'Rejected' }
  ];

  constructor(
    private _route: Router
  ) { }

  ngOnInit(): void {
    this.isLoadingPage = true;
  }

  serachPensioner() {
    this.isLoadingPage = false;
  }

  viewPensioner(index,nic) {
    console.log("index : " + index + ", nic : " + nic);
    this._route.navigateByUrl('/view-account-details');
  }

  editPensioner() {
    this._route.navigateByUrl('/edit-account-details');
  }

  saveAccDetails() {
    // save service
    // alert('New Account Details Successfully Saved!')
    // this._route.navigateByUrl('/dashboard');
  }

  generateReport(){
    // report generate service ,
  this._route.navigateByUrl('/source-document');
  }
}

import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'src/app/models/menu.item';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  menuItems: MenuItem[] = [
    {name: 'Dashboard', pathTo: '/dashboard'},
    {name: 'Search Pensioner', pathTo: '/search-pensioner'},
    {name: 'Report', pathTo: '/account-details-report'}
  ];

  constructor() { }

  ngOnInit(): void {
  }

}

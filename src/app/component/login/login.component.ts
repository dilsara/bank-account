import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { Router } from '@angular/router';

/**
 * @author Nuwan Dilsara
 * ICT Assistant - Department of Pension
 * 2020.05.15
 */


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  user: User[] = [
    {username : '1' , password : '1'}
  ];

  constructor(
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  login(){
    this.router.navigateByUrl('/main');
  }

}

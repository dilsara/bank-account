import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-account-details',
  templateUrl: './edit-account-details.component.html',
  styleUrls: ['./edit-account-details.component.css']
})
export class EditAccountDetailsComponent implements OnInit {

  isLoadingPage: boolean;

  constructor(
    private _route: Router,
    private _toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.isLoadingPage = false;
  }

  
  updatecDetails() {
    this._toastr.success('Successfully update Account Details.', 'Success!');
    this._route.navigateByUrl('/dashboard');
  }

  back(){
    this._route.navigateByUrl('/list-account');
  }

}

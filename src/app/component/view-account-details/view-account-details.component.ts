import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-account-details',
  templateUrl: './view-account-details.component.html',
  styleUrls: ['./view-account-details.component.css']
})
export class ViewAccountDetailsComponent implements OnInit {

  isLoadingPage: boolean;

  constructor(
    private _route: Router
  ) { }

  ngOnInit(): void {
    this.isLoadingPage = false;
  }

  
  close() {
    this._route.navigateByUrl('/dashboard');
  }

  back(){
    this._route.navigateByUrl('/list-account');
  }

}

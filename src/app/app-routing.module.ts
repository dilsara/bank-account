import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './component/login/login.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { MainComponent } from './component/main/main.component';
import { SearchPensionerComponent } from './component/search-pensioner/search-pensioner.component';
import { PensionerDetailsComponent } from './component/pensioner-details/pensioner-details.component';
import { NewAccountDetailsComponent } from './component/new-account-details/new-account-details.component';
import { ListAccountComponent } from './component/list-account/list-account.component';
import { EditAccountDetailsComponent } from './component/edit-account-details/edit-account-details.component';
import { ViewAccountDetailsComponent } from './component/view-account-details/view-account-details.component';
import { AccountDetailsReportComponent } from './component/reports/account-details-report/account-details-report.component';
import { SourceDocumentComponent } from './component/reports/source-document/source-document.component';


const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'main', component: MainComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'search-pensioner', component: SearchPensionerComponent },
  { path: 'pensioner-details', component: PensionerDetailsComponent },
  { path: 'new-account-details', component: NewAccountDetailsComponent },
  // { path: 'list-account/:status', component: ListAccountComponent },
  { path: 'list-account', component: ListAccountComponent },
  { path: 'edit-account-details', component: EditAccountDetailsComponent },
  { path: 'view-account-details', component: ViewAccountDetailsComponent },
  { path: 'account-details-report', component: AccountDetailsReportComponent },
  { path: 'source-document', component: SourceDocumentComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

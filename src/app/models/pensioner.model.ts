export class PensionerModel {
    pid: number;
    name: string;
    nic: string;
    penno: string;
    bank: Bank;
    branch: Branch;
    accountNo: string;
}

class Bank {
    id: number;
    name: string;
}

class Branch {
    id: number;
    name: string;
}
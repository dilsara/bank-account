export class SidebarConfig {

    sidebarConfig: Object[];

    constructor() {

    }

    getSidebarConfig(userRole: string) {
        if (userRole == 'BANK_ACCOUNT_CHANGER') {
            this.sidebarConfig = [
                { name: 'Dashboard', pathTo: '/dashboard' },
                { name: 'Search Pensioner', pathTo: '/search-pensioner' }
            ]
        }
        if (userRole == 'ACCOUNTANT_ACCOUNT_CHANGER_DOP') {
            this.sidebarConfig = [
                { name: 'Dashboard', pathTo: '/dashboard' },
                { name: 'Search Pensioner', pathTo: '/search-pensioner' },
                { name: 'Report', pathTo: '/account-details-report' }
            ]
        }

    }

}
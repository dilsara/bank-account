/**
 * @author Nuwan Dilsara
 * ICT Assistant - Department of Pension
 * 2020.05.15
 */

export class API{
    BASE_SERVICE = 'http://192.168.100.123:8080/bank-account-api/'; // Testing Server

    GET_BANKS = this.BASE_SERVICE + 'bankaccount/loadbank';
}